class CampainPdf < Prawn::Document
	def initialize(campain, date_range)
		super(:page_size => "A4", :margin => 20, :top_margin => 20, :bottom_margin => 50)
    @campain = campain
    @number_of_rows = 24
    font_families.update("Infinity" => {:normal => "#{Rails.root}/app/assets/stylesheets/Infinity.ttf"})
    font_size(8)
    
    #logo Cliente
    if !@campain.user.logo.path.nil?
      image @campain.user.logo.path.to_s, :width => 159, :height => 118.5, :vposition => 4
      move_down 60
    else
      move_down 178.5
    end
    
    # logo Kanamobi
    image "#{Rails.root}/app/assets/images/kanamobi_logo.png", :position => :right, :scale => 0.16, :vposition => 4
    fill_color "CCCCCC" 
    fill_rectangle [285, 721], 268, 0.5
    
    # Campain
    fill_color "392a57" 
    fill_rectangle [285, 697], 268, 24
    fill_color "FFFFFF" 
    text_box "#{self.campain_type(@campain)} | #{@campain.name}", :at => [285, 688], :width => 268, :align => :center

    # Date Range
    fill_color "f5f5f5" 
    fill_rectangle [285, 673], 268, 24
    fill_color "6E6E6E" 
    fill_rectangle [425, 669], 122, 15
    fill_color "D6D6D6" 
    text_box "#{date_range}", :at => [425, 665.2], :width => 122, :align => :center

    # text "#{campain_type(@campain)} | #{@campain.name}", :position => :right
    # text "Impactos"
    # text "#{@campain.infos.count}"
    # text "Dias"
    # text "#{get_days(@campain.id)}"

    campain_id = @campain.id
    
    if @campain.campain_type == 1
      # Box pink
      fill_color "661466" 
      fill_rectangle [0, 645], 105, 88.5
      
      # Box cinza
      fill_color "f5f5f5" 
      fill_rectangle [110, 645], 95, 88.5
      fill_rectangle [210, 645], 130, 88.5
      fill_rectangle [345, 645], 130, 88.5
      fill_rectangle [480, 645], 73, 88.5

      # line purple
      fill_color "392a57" 
      fill_rectangle [110, 645], 95, 2.5
      fill_rectangle [210, 645], 130, 2.5
      fill_rectangle [345, 645], 130, 2.5
      fill_rectangle [480, 645], 73, 2.5  

      image "#{Rails.root}/public/charts/#{campain_id}/delivered.png", :at => [110, 635], :width => 95
      image "#{Rails.root}/public/charts/#{campain_id}/region.png", :at => [210, 635], :width => 130
      image "#{Rails.root}/public/charts/#{campain_id}/operator.png", :at => [345, 635], :width => 130

      fill_color "FFFFFF"
      text_box "IMPACTOS", :at => [0, 638], :width => 105, :align => :center
      font_size(23)
      text_box "#{@campain.infos.count}", :at => [0, 610], :width => 105, :align => :center

      fill_color "392a57"
      text_box "#{get_days(@campain.id)}", :at => [480, 610], :width => 73, :align => :center
      font_size(8)
      text_box "ENTREGUES", :at => [110, 638], :width => 105, :align => :center
      text_box "REGIÕES", :at => [210, 638], :width => 130, :align => :center
      text_box "OPERADORAS", :at => [345, 638], :width => 130, :align => :center
      text_box "DIAS", :at => [480, 638], :width => 73, :align => :center

    else
      
      @total_clicks = @campain.infos.map { |i| i.clicks }.sum
      @total_prints = @campain.infos.map { |i| i.prints }.sum if !@campain.infos.first.prints.nil?
      @total_broadcast = @campain.infos.map { |i| i.broadcast }.sum
      if @total_broadcast > 0
        @clicks_per_broadcast = (@total_clicks / @total_broadcast).to_i
      else
        @clicks_per_broadcast = 0
      end

      # Box pink
      fill_color "661466" 
      fill_rectangle [0, 645], 117, 88.5
      
      # Box cinza
      fill_color "f5f5f5" 
      fill_rectangle [122, 645], 78, 88.5
      fill_rectangle [205, 645], 130, 88.5
      fill_rectangle [340, 645], 130, 88.5
      fill_rectangle [475, 645], 78, 88.5

      # line purple
      fill_color "392a57" 
      fill_rectangle [122, 645], 78, 2.5
      fill_rectangle [205, 645], 130, 2.5
      fill_rectangle [340, 645], 130, 2.5
      fill_rectangle [475, 645], 78, 2.5  

       image "#{Rails.root}/public/charts/#{campain_id}/region.png", :at => [340, 635], :width => 130

      fill_color "FFFFFF"

      if !@total_prints.nil?
        text_box "TOTAL DE IMPRESSÕES", :at => [0, 638], :width => 117, :align => :center
      else
        text_box "TOTAL DE CLICKS", :at => [0, 638], :width => 117, :align => :center
      end

      font_size(23)
      
      if !@total_prints.nil?
        text_box "#{@total_prints}", :at => [0, 610], :width => 105, :align => :center
      else
        text_box "#{@total_clicks}", :at => [0, 610], :width => 105, :align => :center
      end

      fill_color "392a57"
      text_box "#{@total_broadcast}", :at => [122, 610], :width => 78, :align => :center
      text_box "#{@clicks_per_broadcast}", :at => [205, 610], :width => 130, :align => :center
      text_box "#{get_days(@campain.id)}", :at => [475, 610], :width => 78, :align => :center
      font_size(8)
      text_box "BROADCASTS", :at => [122, 638], :width => 78, :align => :center
      text_box "CLICKS POR BROADCAST", :at => [205, 638], :width => 130, :align => :center
      text_box "REGIÕES", :at => [340, 638], :width => 130, :align => :center
      text_box "DIAS", :at => [475, 638], :width => 78, :align => :center
      
    end
    
    fill_color "000000"

		line_items(@campain)
		
    page_count.times do |i| 
      go_to_page(i+1) 
      fill_color "392a57" 
      fill_rectangle [0,6], 553, 3 
      fill_color "000000" 
      fill_rectangle [0,-20], 553, 12 
      fill_color "392a57" 
      fill_rectangle [0,-8], 553, 12 
      self.fill_color "FFFFFF" 
      draw_text "Kanamobi @ #{Time.now.year}", :at => [bounds.left + 8, bounds.bottom - 16], :width => 200, :size => 6.2
      draw_text "PÁGINA #{i+1} de #{page_count()}", :at => [bounds.left + 8, bounds.bottom - 28], :width => 200, :size => 6.2 
      draw_text "Desenvolvido por", :at => [bounds.right - 83.5, bounds.bottom - 16], :width => 200, :size => 6.2
      draw_text "www.10i9.com.br", :at => [bounds.right - 58, bounds.bottom - 28], :width => 200, :size => 6.2
      # logo 10i9
      image "#{Rails.root}/app/assets/images/10i9.png", :scale => 0.46, :at => [bounds.right - 33.5, bounds.bottom - 9.5]

    end   
  end 

	def line_items(campain)
    font_size(7)
    if campain.campain_type == 1
      table line_item_rows do
        row(0).style(:background_color => '392a57', :text_color =>'ffffff', :height => 12, :padding => [2, 0, 0, 0], :border_width => [0,0,0,0.5], :border_color => "FFFFFF", :align => :center )
        self.row_colors = ["FFFFFF", "DDDDDD"]
        row(1..campain.infos.limit(@number_of_rows).count).style(:border_width => [0,0,0.5,0.5], :padding => [2, 0, 4, 0], :border_color => "CCCCCC", :align => :center, :valign => :center )
        column(0).style(:border_width => [0,0,0.5,0], :align => :left, :padding => [2, 0, 4, 5] )
        column(3).style(:align => :left, :padding => [2, 10, 4, 10] )
        cells[0,0].style(:padding => [2, 5, 0, 8])
        cells[0,3].style(:padding => [2, 5, 0, 8])
        self.header = true
        # self.column_widths = [70, 42, 33, 260, 38, 55, 38]
        self.column_widths = [70, 42, 33, 260, 43, 62, 43]
        self.width = 553
      end
    else
      table line_item_rows do
        row(0).style(:background_color => '392a57', :text_color =>'ffffff', :height => 12, :padding => [2, 0, 0, 0], :border_width => [0,0,0,0.5], :border_color => "FFFFFF", :align => :center )
        self.row_colors = ["FFFFFF", "DDDDDD"]
        row(1..campain.infos.limit(@number_of_rows).count).style(:border_width => [0,0,0.5,0.5], :padding => [2, 0, 4, 0], :border_color => "CCCCCC", :align => :center, :valign => :center )
        column(0).style(:border_width => [0,0,0.5,0], :align => :left, :padding => [2, 0, 4, 5] )
        column(2).style(:align => :left, :padding => [2, 10, 4, 10] )
        cells[0,0].style(:padding => [2, 5, 0, 8])
        cells[0,2].style(:padding => [2, 5, 0, 8])
        self.header = true
        
        if !campain.infos.first.prints.nil?
          self.column_widths = [40, 35, 287, 58, 38, 42, 53]
        else
          self.column_widths = [40, 35, 340, 58, 38, 42]
        end

        self.width = 553
      end
    end
  end
  
  def line_item_rows
    if @campain.campain_type == 1
      [["TELEFONE", "DATA", "HORA", "FRASEOLOGIA", "STATUS", "OPERADORA", "REGIÃO"]] +
      @campain.infos.limit(@number_of_rows).map do |info|
        [info.impact, info.date.strftime("%d/%m/%y"), info.date.strftime("%H:%M"), info.phraseology, info.status, info.operator, Region.find(info.region_id).name]
      end  
    
    elsif !@campain.infos.first.prints.nil?
      [["DATA", "HORA", "FRASEOLOGIA", "BROADCAST", "CLICKS", "REGIÃO", "IMPRESSÃO"]] +
      @campain.infos.limit(@number_of_rows).map do |info|
        [info.date.strftime("%d/%m/%y"), info.hour, info.phraseology, info.broadcast, info.clicks, Region.find(info.region_id).name, info.prints]
      end
    
    else
      [["DATA", "HORA", "FRASEOLOGIA", "BROADCAST", "CLICKS", "REGIÃO"]] +
      @campain.infos.limit(@number_of_rows).map do |info|
        [info.date.strftime("%d/%m/%y"), info.hour, info.phraseology, info.broadcast, info.clicks, Region.find(info.region_id).name]
      end
    end
    
  end

  def campain_type(campain)
    if campain.campain_type == 1
      "SMS"
    else
      "GEO MÍDIA"
    end
  end

  def get_days(campain_id)
    maxDate = Info.find_all_by_campain_id(campain_id).map { |i| i.date }.max
    minDate = Info.find_all_by_campain_id(campain_id).map { |i| i.date }.min
    if !maxDate.nil? && !minDate.nil?
      diference = ((maxDate - minDate) / 1.day).to_i
      if diference == 0
        @days = 1
      else
        @days = diference
      end
    end
  end
end
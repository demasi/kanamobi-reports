module ApplicationHelper

	def get_formatted_date(date)
		date.strftime("%d/%m/%y")
	end

	def get_formatted_time(date)
		date.strftime("%H:%M")
	end

	def get_region(region_id)
		Region.find(region_id).name
	end
end

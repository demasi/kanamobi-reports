class Info < ActiveRecord::Base
  attr_accessible :broadcast, :campain_id, :clicks, :date, :hour, :impact, :operator, :phraseology, :region_id, :status

  belongs_to :campain
  has_one :region

end

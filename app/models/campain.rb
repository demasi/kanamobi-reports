class Campain < ActiveRecord::Base
  attr_accessible :from, :name, :to, :campain_type, :user_id

  validates_presence_of :campain_type, :message => "Esse campo é obrigatório"
  validates_presence_of :name, :message => "Nome da campanha não informado."
  validates_uniqueness_of :name, :message => "Já existe uma campanha com esse nome"
  validates_presence_of :from, :message => "Data inválida."
  validates_presence_of :to, :message => "Data inválida."

  has_many :sheets, :dependent => :destroy
  has_many :infos, :dependent => :destroy
  belongs_to :user

  def to_csv
  	
  end
end

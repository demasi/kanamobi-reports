class Sheet < ActiveRecord::Base
  attr_accessible :campain_id, :path

  # validate :is_same_name?
  validates_presence_of :path, :message => "Esse campo é obrigatório"
  validates_uniqueness_of :path, :message => "Já existe uma planilha enviada com esse nome"
  belongs_to :campain

  mount_uploader :path, SheetUploader

  def is_same_name?
  	if path.path.nil?
  		return
  	end
  	puts "FILENAME: #{path.file.original_filename}"
  	sheets = Sheet.all
  	sheets.each do |s|
  		if s.path.file.original_filename == path.file.original_filename
  			errors.add(:path, "Já existe uma planilha enviada com esse nome")
  		end
  	end
  end
end

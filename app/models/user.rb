class User < ActiveRecord::Base
  attr_accessible :contact, :email, :logo, :name, :role, :password, :password_confirmation
  attr_accessor :password

  validates_confirmation_of :password, :message => "As senhas não conferem."
  validates_presence_of :password, :on => :create, :message => "Senha não informada."
  # validates_presence_of :email, :message => "Esse campo é obrigatório."
  # validates_uniqueness_of :email, :message => "E-mail já registrado no sistema."
  # validates_presence_of :name, :message => "Nome do cliente não informado."
  # validates_uniqueness_of :name, :message => "Cliente já registrado no sistema." 
  before_save :encrypt_password

  has_many :campains, :dependent => :destroy

  mount_uploader :logo, ImageUploader

  # Metodo de autenticacao
  def self.authenticate(email, password)
  	user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
     user
    else
     nil
   end
  end

  def encrypt_password
     if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
end

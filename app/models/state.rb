class State < ActiveRecord::Base
  attr_accessible :alias, :ddd, :name, :region_id

  belongs_to :region
end

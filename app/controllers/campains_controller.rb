 require 'spreadsheet'
class CampainsController < ApplicationController

  before_filter :logged_in?

  # GET /campains
  # GET /campains.json
  def index
    @campains = Campain.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @campains }
    end
  end

  # GET /campains/1
  # GET /campains/1.json
  def show
    @number_of_rows = 30
    @campain = Campain.find(params[:id])
    @infos = @campain.infos.limit(@number_of_rows)
    @campain_type = @infos.first.campain.campain_type if !@infos.empty?
    session[:campain_id_for_select_tag] = @campain.id

    @showSMS = @campain.campain_type == 1 && !@infos.empty?
    @showLivescreen = @campain.campain_type == 2 && !@infos.empty?

    puts "SHOW SMS: #{@showSMS}"
    puts "SHOW LIVESCREEN: #{@showLivescreen}"
    puts "session > #{session[:date_range]}"

    session[:date_range2] = "#{@campain.user.campains.minimum("from").strftime("%d/%m/%Y")} A #{@campain.user.campains.maximum("to").strftime("%d/%m/%Y")}"


    if @campain.campain_type == 1
      get_chart_data_sms(@campain)
    else
      get_chart_data_livescreen(@campain.id)
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @campain }
      format.csv { render :text => @campain.to_csv }
      format.pdf do
        pdf = CampainPdf.new(@campain, session[:date_range])
        send_data pdf.render, :filename => "#{@campain.name}",
                              :type => "application/pdf",
                              :disposition => "inline"
      end
    end
  end

  # GET /campains/new
  # GET /campains/new.json
  def new

    # puts "PARAMNS: <#{params[:user]}>"

    # if !params[:user].nil?
    #  @@user = User.find(params[:user])  
    # end
    puts "USUARIO CAMPANHA: #{User.find(session[:user_id_for_select_tag]).name}"
    @campain = Campain.new
    # @user_id = @@user.id

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @campain }
    end
  end

  # GET /campains/1/edit
  def edit
    @campain = Campain.find(params[:id])
    @user_id = @campain.user.id
  end

  # POST /campains
  # POST /campains.json
  def create
    @campain = Campain.new(params[:campain])

    @campain.user_id = session[:user_id_for_select_tag]
    
    puts params[:campain]

    respond_to do |format|
      if @campain.save
        @@user = nil
        format.html { redirect_to @campain, :notice => 'Campanha criada com sucesso.' }
        format.json { render :json => @campain, :status => :created, :location => @campain }
        format.js
      else
        format.html { render :action => "new" }
        format.json { render :json => @campain.errors, :status => :unprocessable_entity }
        format.js
      end
    end
  end

  # PUT /campains/1
  # PUT /campains/1.json
  def update
    @campain = Campain.find(params[:id])

    respond_to do |format|
      if @campain.update_attributes(params[:campain])
        format.html { redirect_to @campain, :notice => 'Campanha atualizada com sucesso.' }
        format.json { head :no_content }
        format.js
      else
        format.html { render :action => "edit" }
        format.json { render :json => @campain.errors, :status => :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /campains/1
  # DELETE /campains/1.json
  def destroy
    @campain = Campain.find(params[:id])
    @campain.destroy
    session[:campain_id_for_select_tag] = nil

    redirect_to current_user
  end

  def get_chart_data_sms(campain)
    @campainTableHelper = CampainTableHelper.find_by_campain_id(campain.id)


    if @campainTableHelper.nil?
      @campainTableHelper = CampainTableHelper.new(:campain_id => campain.id, :impacts => 0, :delivered => 0, :undelivered => 0,
      :norte => 0, :sul => 0, :nordeste => 0, :centro_oeste => 0, :sudeste => 0, :tim => 0, :vivo => 0, :oi => 0, :claro => 0)
    end

    @delivered = @campainTableHelper.delivered
    @undelivered = @campainTableHelper.undelivered

    # @operators = Info.find_all_by_campain_id(campain_id).map {|e| e.operator}.uniq
    op = []
    if !@campainTableHelper.tim != 0
      op << "Tim"
    end
    if !@campainTableHelper.oi != 0
      op << "Oi"
    end
    if !@campainTableHelper.vivo != 0
      op << "Vivo"
    end
    if !@campainTableHelper.claro != 0
      op << "Claro"
    end


    @norte = @campainTableHelper.norte
    @nordeste = @campainTableHelper.nordeste
    @centro_oeste = @campainTableHelper.centro_oeste
    @sudeste = @campainTableHelper.sudeste
    @sul = @campainTableHelper.sul

    @tim = @campainTableHelper.tim
    @oi = @campainTableHelper.oi
    @claro = @campainTableHelper.claro
    @vivo = @campainTableHelper.vivo


    # maxDate = campain.infos.map { |i| i.date }.max
    # minDate = campain.infos.map { |i| i.date }.min
    # if !maxDate.nil? && !minDate.nil?
    #   diference = ((maxDate - minDate) / 1.day).to_i
    #   if diference == 0
    #     @days = 1
    #   else
    #     @days = diference
    #   end
    # end

    @days = @campainTableHelper.date_range

  end

  def get_chart_data_livescreen(campain_id)
    @norte = Info.find_all_by_campain_id_and_region_id(campain_id, 1).count
    @nordeste = Info.find_all_by_campain_id_and_region_id(campain_id, 2).count
    @centro_oeste = Info.find_all_by_campain_id_and_region_id(campain_id, 3).count
    @sudeste = Info.find_all_by_campain_id_and_region_id(campain_id, 4).count
    @sul = Info.find_all_by_campain_id_and_region_id(campain_id, 5).count

    infos = Info.find_all_by_campain_id(campain_id)
    @total_clicks = infos.map { |i| i.clicks }.sum
    @total_prints = infos.map { |i| i.prints }.sum if !infos.first.nil? && !infos.first.prints.nil?
    @total_broadcast = infos.map { |i| i.broadcast }.sum
    if @total_broadcast > 0
      @clicks_per_broadcast = (@total_clicks / @total_broadcast).to_i
    else
      @clicks_per_broadcast = 0
    end
    maxDate = infos.map { |i| i.date }.max
    minDate = infos.map { |i| i.date }.min
    if !maxDate.nil? && !minDate.nil?
      diference = ((maxDate - minDate) / 1.day).to_i
      if diference == 0
        @days = 1
      else
        @days = diference
      end
    end
  end

  def filter_campains_by_date

          
    if !session[:user_id_for_select_tag].nil?
      user = User.find(session[:user_id_for_select_tag])

      f = params[:from]
      from = Date.strptime(f[2]+"-"+f[1]+"-"+f[0], "%Y-%m-%d")

      t = params[:to]
      to = Date.strptime(t[2]+"-"+t[1]+"-"+t[0], "%Y-%m-%d")

      session[:date_range2] = "#{f[2]+"-"+f[1]+"-"+f[0]} A #{t[2]+"-"+t[1]+"-"+t[0]}"

      # Adicionado um dia pois ao realizar a consulta o ultimo dia nao eh incluido na pesquisa (exclusivo)
      campains = Campain.all(:conditions => {:user_id => user.id, :from => from..(to + 1.day)})
      @c = campains.map { |cp| ["#{campain_type(cp)} | #{cp.name}" , cp.id] }.insert(0, ["Selecione uma campanha", 0])
      
      render :partial => "/users/select_tag_campain"
    end

  end

  def export_spreadsheet
    campain = Campain.find(params[:id])
    infos = campain.infos
    Spreadsheet.client_encoding = 'UTF-8'
    path = "#{Rails.root}/public/#{campain.id}.xls"

    book = Spreadsheet::Workbook.new
    sheet1 = book.create_worksheet
    sheet1.name = 'My First Worksheet'

    if campain.campain_type == 1
      sheet1.row(0).concat %w{Contato DataHora Fraseologia Estado Operadora}
      sheet1.column(0).width=15
      sheet1.column(1).width=18
      sheet1.column(2).width=40
      sheet1.column(3).width=10
      sheet1.column(4).width=10
      for i in 0..infos.count-1
        sheet1.row(i+1).concat [infos[i].impact, infos[i].date.strftime("%d/%m/%Y %H:%M"), infos[i].phraseology, infos[i].status, infos[i].operator]
      end
    else
      if !infos.first.prints.nil?
        sheet1.row(0).concat %w{DateTime Program Broadcast Clicks Region Prints}
        sheet1.column(0).width=18
        sheet1.column(1).width=40
        sheet1.column(2).width=10
        sheet1.column(3).width=10
        sheet1.column(4).width=10
        sheet1.column(5).width=10
        for i in 0..infos.count-1
          sheet1.row(i+1).concat ["#{infos[i].date.strftime("%d/%m/%Y")} #{infos[i].hour}", infos[i].phraseology, infos[i].broadcast, infos[i].clicks, Region.find(infos[i].region_id).name, infos[i].prints]
        end
      else
        sheet1.row(0).concat %w{DateTime Program Broadcast Clicks Region}
        sheet1.column(0).width=18
        sheet1.column(1).width=40
        sheet1.column(2).width=10
        sheet1.column(3).width=10
        sheet1.column(4).width=10
        for i in 0..infos.count-1
          sheet1.row(i+1).concat ["#{infos[i].date.strftime("%d/%m/%Y")} #{infos[i].hour}", infos[i].phraseology, infos[i].broadcast, infos[i].clicks, Region.find(infos[i].region_id).name]
        end
      end
    end
    
    book.write path

    send_file(path, :filename => "#{campain.name}.xls")

    # FileUtils.rm("#{Rails.root}/public/#{campain.id}.xls")

  end

  def show_date_filter
    session[:user_id_for_select_tag] = params[:id]

    render :partial => "date_filter"
  end

  def show_date_filter_by_user_name
    user = User.find_by_name(params[:name])

    session[:user_id_for_select_tag] = user.id
    render :partial => "date_filter"
  end

  def get_svg
    @campain = Campain.find(session[:campain_id_for_select_tag])
    campain_id = @campain.id
    FileUtils.mkdir_p "#{Rails.root}/public/charts/#{campain_id}"
    path = "#{Rails.root}/public/charts/#{campain_id}/"

    session[:date_range] = "#{params[:start]} A #{params[:end]}"


    if @campain.campain_type == 1 
      File.open("#{path}/delivered.svg", 'w') {|f| f.write(params[:delivered]) }
      File.open("#{path}/region.svg", 'w') {|f| f.write(params[:region]) }
      File.open("#{path}/operator.svg", 'w') {|f| f.write(params[:operator]) }

      ["#{path}/delivered.svg","#{path}/region.svg","#{path}/operator.svg"].each do |file|
        Dir.chdir("#{Rails.root}/public/batik/") do
            # Verificar se o Java esta instalado na maquina
            # Checar local de instalacao do Java
            retResult  = system("java -jar batik-rasterizer.jar #{file} -d #{Rails.root}/public/charts/#{campain_id}")

            puts retResult
         end #chdir
      end

    else
      File.open("#{path}/region.svg", 'w') {|f| f.write(params[:region]) }
      Dir.chdir("#{Rails.root}/public/batik/") do
            # Verificar se o Java esta instalado na maquina
            # Checar local de instalacao do Java
            retResult  = system("java -jar batik-rasterizer.jar #{path}/region.svg -d #{Rails.root}/public/charts/#{campain_id}")

            puts retResult
         end #chdir
    end

    redirect_to @campain, :format => :pdf
  end

  def get_date_range
    render :partial => "date_range"
  end
end

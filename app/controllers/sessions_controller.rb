class SessionsController < ApplicationController
  
  before_filter :not_logged_in?, :except => :destroy
  
  def new
  end

  def create
  	user = User.authenticate(params[:email], params[:password])
  	if user
  		session[:user_id] = user.id
  		redirect_to user
  	else
  		flash.now.alert = "Email ou senha inválido(s)"
  		render :new
  	end
  end

  def destroy
    session[:user_id] = nil
    session[:user_id_for_select_tag] = nil
    session[:campain_id_for_select_tag] = nil
    redirect_to root_url
  end
end

class UsersController < ApplicationController

	before_filter :logged_in?, :except => [:forgot_password, :send_password_email]

	def new
	  @user = User.new
	end

	def create
	  @user = User.new(params[:user])
	  respond_to do |format|
      if @user.save
      	# UserMailer.user_created(@user, "Registro em Kanamobi").deliver
      	session[:campain_id_for_select_tag] = nil
      	session[:user_id_for_select_tag] = @user.id
        format.html { redirect_to(@user, :notice => 'Usuário criado com sucesso.') }
        format.xml  { render :xml => @user, :status => :created, :location => @user }
        format.js
      else
        format.html { render "new" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
        format.js
      end
    end
	end

	def show
		@user = User.find(params[:id])
		@c = []
		if !session[:campain_id_for_select_tag].nil?
			campain = Campain.find(session[:campain_id_for_select_tag])
			redirect_to campain_path(campain)
		end
	end

	def edit
		@user = User.find(params[:id])
	end

	def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { 
        	if session[:campain_id_for_select_tag].nil?
        		redirect_to @user
        	else
        		campain = Campain.find(session[:campain_id_for_select_tag])
        		redirect_to campain_path(campain)
        	end
        }
        format.json { head :no_content }
        format.js
      else
        format.html { render :action => "edit" }
        format.json { render :json => @user.errors, :status => :unprocessable_entity }
        format.js
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    logo_path = "#{Rails.root}/public/uploads/user/logo/#{@user.id}"
    FileUtils.rm_rf logo_path
    @user.destroy

    redirect_to current_user
  end

	# pagina de 'Esqueci a senha'
	def forgot_password
		
	end

	# acao de enviar email
	def send_password_email
		user = User.find_by_email(params[:email])
	
		if user
			tokens =  [('a'..'z'),(0..9)].map{|i| i.to_a}.flatten
			new_password =  (0...5).map{ tokens[rand(tokens.length)] }.join

			user.update_attributes(:password => new_password)

			UserMailer.user_created(user, "Kanamobi Reports - Reenvio de informações de conta").deliver
			redirect_to root_url, :notice => "Email enviado com sucesso"
		else
			flash[:alert] = "Email inválido"
			redirect_to root_url
		end
	end

	def associate_user_to_campain
		if params[:first]
			puts "AAAAAAAAAAAA"
		elsif params[:second]
			puts "BBBBBBBBBBBB"
		end

		user = User.find(params[:user])
		campain = Campain.find(params[:campain])

		campain.update_attributes(:user_id => user.id)
	end

	def get_data
		user = User.find(params[:user])

		redirect_to :controller => :campains, :action => :new, :user => user.id
	end

	def select_user_campain
		user = User.find(params[:id])
		session[:user_id_for_select_tag] = user.id
		@c = user.campains.map { |campain| ["#{campain_type(campain)} | #{campain.name}", campain.id] }.insert(0, ["Selecione uma campanha", 0])
		render :partial => "select_tag_campain"
	end

	def redirect_to_user_path

		if !session[:user_id_for_select_tag].nil?
			session[:user_id_for_select_tag] = nil
			session[:campain_id_for_select_tag] = nil
			respond_to do |format|
				format.js { render :js => "window.location.href = '/users/#{current_user.id}'" }
			end

		end
	end

	def select_user_name
		@user = User.find(params[:id])
		session[:user_id_for_select_tag] = @user.id

		render :partial => "client_name"
		
	end

	def select_user_logo
		@user = User.find(params[:id])
		session[:user_id_for_select_tag] = @user.id
		
		render :partial => "client_logo"
	end

  def delete_logo
  	user = User.find(params[:id])
		
		logo_path = "#{Rails.root}/public/uploads/user/logo/#{user.id}"
    FileUtils.rm_rf logo_path
    user.update_attributes(:logo => nil)

  end

  def redirecionar
  	user = User.find(params[:id])
  	session[:campain_id_for_select_tag] = nil
  	session[:user_id_for_select_tag] = user.id
  	respond_to do |format|
				format.js { render :js => "window.location.href = '/users/#{user.id}'" }
		end
  end

  def update_password
  	@user = current_user
  	render "edit_password"
  end

  def update_password_action
  	password_salt = BCrypt::Engine.generate_salt
    password_hash = BCrypt::Engine.hash_secret(params[:password], current_user.password_salt)
    @error = nil
    @error_new_password = nil

    if current_user.password_hash != password_hash
    	@error = "Senha incorreta"
    elsif params[:new_password] == "" || params[:new_password_confirmation] == ""
    	@error_new_password = "Nova senha inválida"
    else
    	current_user.update_attributes(:password => params[:new_password])
    end

    respond_to do |format|
    	format.js
    end

  end
	
end

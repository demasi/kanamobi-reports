class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user

  private

  def current_user
  	@current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    if current_user.nil?
      redirect_to("/log_in", :alert => "Você precisa se autenticar para ter acesso a esse recurso")
    end
  end

  def not_logged_in?
    if !current_user.nil?
      redirect_to(current_user)
    end
  end
  
  def campain_type(campain)
    if campain.campain_type == 1
      "SMS"
    else
      "GEO MÍDIA"
    end
  end

end

require 'roo'

class SheetsController < ApplicationController

  before_filter :logged_in?

  # GET /sheets
  # GET /sheets.json
  def index
    @sheets = Sheet.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @sheets }
    end
  end

  # GET /sheets/1
  # GET /sheets/1.json
  def show
    @sheet = Sheet.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @sheet }
    end
  end

  # GET /sheets/new
  # GET /sheets/new.json
  def new
    @sheet = Sheet.new
    @@campain_id = params[:campain]

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @sheet }
    end
  end

  # GET /sheets/1/edit
  def edit
    @sheet = Sheet.find(params[:id])
  end

  # POST /sheets
  # POST /sheets.json
  def create
    @sheet = Sheet.new(params[:sheet])
    @sheet.campain_id = session[:campain_id_for_select_tag]
    campain = Campain.find(session[:campain_id_for_select_tag])

    respond_to do |format|
      if @sheet.save
        format.html { redirect_to campain, :notice => 'Planilha importada com sucesso.' }
        format.json { render :json => @sheet, :status => :created, :location => @sheet }
        format.js

        @spreadsheet = self.get_spreadsheet(@sheet.path.file.path)
        @spreadsheet.default_sheet = @spreadsheet.sheets.first

        @first_column = @spreadsheet.first_column
        @last_column = @spreadsheet.last_column
        @first_row = @spreadsheet.first_row
        @last_row = @spreadsheet.last_row

        
        if campain.campain_type == 1 #======== SMS =========#
          self.parse_sms
        else #======== LiveScreen =========#
          self.parse_livescreen
        end

      else
        # format.html { render :action => "new" }
        # format.json { render :json => @sheet.errors, :status => :unprocessable_entity }
        format.js
      end
    end

    
  end

  # PUT /sheets/1
  # PUT /sheets/1.json
  def update
    @sheet = Sheet.find(params[:id])

    respond_to do |format|
      if @sheet.update_attributes(params[:sheet])
        format.html { redirect_to @sheet, :notice => 'Sheet was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @sheet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /sheets/1
  # DELETE /sheets/1.json
  def destroy
    @sheet = Sheet.find(params[:id])
    @sheet.destroy

    respond_to do |format|
      format.html { redirect_to sheets_url }
      format.json { head :no_content }
    end
  end

  def get_spreadsheet(file)
    puts "FILE >>>>> #{file}"
    file_extension = File.extname(file)
    spreadsheet = nil

    if file_extension.eql? ".xlsx"
      spreadsheet = Excelx.new(file)
    elsif file_extension.eql? ".xls"
      spreadsheet = Excel.new(file)
    end

    spreadsheet
  end

  def parse_sms

    impactColumn = nil
    dateColumn = nil
    hourColumn = nil
    statusColumn = nil
    phraseologyColumn = nil
    operatorColumn = nil


    # impact=Array.new
    # date=Array.new
    # hour=Array.new
    # status=Array.new
    # phraseology=Array.new
    # operator=Array.new
    infos = Array.new

   #Validação de colunas
    @first_column.upto(@last_column) do |column|
      header = @spreadsheet.cell(@first_row, column)
      puts "HEADER >>>>> #{header}"
      if header.nil?
        next # Pula a iteracao atual
      end
      if header.casecmp('contato') == 0
          impactColumn = column
      elsif header.casecmp('datahora') == 0
          dateColumn = column
      elsif header.casecmp('data') == 0
          dateColumn = column
      elsif header.casecmp('hora') == 0
          horaColumn = column
      elsif header.casecmp('estado') == 0
          statusColumn = column
      elsif header.casecmp('fraseologia') == 0
          phraseologyColumn = column
      elsif header.casecmp('operadora') == 0
          operatorColumn = column
      end
    end
 
    query = []
    sql = nil
    i = 1
    pages = 1
    hasError = false

    campainHelper = CampainTableHelper.find_by_campain_id(@@campain_id)

    if campainHelper.nil?
      campainHelper = CampainTableHelper.new(:campain_id => @@campain_id, :impacts => 0, :delivered => 0, :undelivered => 0,
      :norte => 0, :sul => 0, :nordeste => 0, :centro_oeste => 0, :sudeste => 0, :tim => 0, :vivo => 0, :oi => 0, :claro => 0)
    end

    if !impactColumn.nil? && !dateColumn.nil? && !statusColumn.nil? && !phraseologyColumn.nil?
      (@first_row+1).upto(@last_row) do |row|
        info = Info.new
 
        info.impact = @spreadsheet.cell(row, impactColumn)
        info.date = @spreadsheet.cell(row, dateColumn)
        info.hour = @spreadsheet.cell(row, hourColumn) if !hourColumn.nil?
        info.status = @spreadsheet.cell(row, statusColumn)
        info.phraseology = @spreadsheet.cell(row, phraseologyColumn)
        if !operatorColumn.nil?
          info.operator = @spreadsheet.cell(row, operatorColumn)
        else
          info.operator = ""            
        end
        
        info.campain_id = @@campain_id
        ddd = info.impact.to_s.match(/\(\d{2}\)/).to_s.gsub('(', '').gsub(')', '')
          # puts "DDD >>> #{ddd.to_s}"
          puts "#{info.impact}"
        if ddd.empty?
          # puts "#{}"
          # puts "RANGE>>>>> #{info.impact.to_s.to_i.to_s[2..3].to_i}"
          state = State.find_by_ddd(info.impact.to_s.to_i.to_s[2..3].to_i)
          info.region_id = state.region.id if !state.nil?
          info.impact = @spreadsheet.cell(row, impactColumn).to_s.to_i.to_s
        else
          state = State.find_by_ddd(ddd.to_s.to_i)
          info.region_id = state.region.id if !state.nil?
        end
                  
        # Verifica se foi encontrado o DDD/Regiao a partir do número de telefone/contato
        if state.nil? 
          
          # Se tiver Status ENTREGUE
          # if info.status.to_s.casecmp('entregue') == 0

            if info.impact.to_s.to_i == 0
              session[:error_sheet] = "Nenhuma informação foi incluída no sistema. Existe impacto vazio na coluna de CONTATO"
            else
              session[:error_sheet] = "Nenhuma informação foi incluída no sistema. O contato #{info.impact} na coluna de CONTATO está inválido"
            end
            hasError = true
            break

          # Caso exista contato inválido, porém com status diferente de Entregue, será aceito
          # else
            # info.region_id = 1;
          # end
        end
  
        # info.save!
        # infos << info
        query << "('#{info.impact}', '#{info.date}', '#{info.hour}', '#{info.status}', '#{info.phraseology}', '#{info.operator}', #{info.region_id}, #{info.campain_id})"

        if info.status.to_s.casecmp('entregue') == 0
          campainHelper.delivered +=1
        else
          campainHelper.undelivered+=1
        end

        if info.region_id == 1
          campainHelper.norte+=1
        elsif info.region_id == 2
          campainHelper.nordeste+=1
        elsif info.region_id == 3
          campainHelper.centro_oeste+=1
        elsif info.region_id == 4
          campainHelper.sudeste+=1
        elsif info.region_id == 5
          campainHelper.sul+=1
        end

        if info.operator.casecmp('tim') == 0
          campainHelper.tim+=1
        elsif info.operator.casecmp('oi') == 0
          campainHelper.oi+=1
        elsif info.operator.casecmp('vivo') == 0
          campainHelper.vivo+=1
        elsif info.operator.casecmp('claro') == 0
          campainHelper.claro+=1
        end

        if i == pages*500 || i == (@last_row-1)
          sql = "INSERT INTO infos (impact, date, hour, status, phraseology, operator, region_id, campain_id) VALUES #{query.join(', ')}"
          ActiveRecord::Base.connection.execute sql
          query = []
          sql = nil
          pages += 1
        end
        i += 1
      end


       # Nao salvar se houve erro durante parser.
      if !hasError
      
          helper = CampainTableHelper.find_by_campain_id(@@campain_id)
          campain = Campain.find(@@campain_id)
    
          maxDate = campain.infos.map { |info| info.date }.max
          minDate = campain.infos.map { |info| info.date }.min
          if !maxDate.nil? && !minDate.nil?
            diference = ((maxDate - minDate) / 1.day).to_i
            if diference == 0
              @days = 1
            else
              @days = diference
            end
          end
    
          campainHelper.impacts = (i-1)
          campainHelper.date_range = @days
    
          if helper.nil?
            campainHelper.save!
          else
            helper.update_attributes(:impacts => (helper.impacts + campainHelper.impacts), :delivered => campainHelper.delivered, :undelivered => campainHelper.undelivered,
              :norte => campainHelper.norte, :sul => campainHelper.sul, :nordeste => campainHelper.nordeste, :sudeste => campainHelper.sudeste, :centro_oeste => campainHelper.centro_oeste,
              :tim => campainHelper.tim, :oi => campainHelper.oi, :vivo => campainHelper.vivo, :claro => campainHelper.claro, :date_range => @days)
          end
      end
    else
      # ERROR!!! Coluna faltando
      session[:error_sheet] = "Nenhuma informação foi incluída no sistema. Verifique se a planilha está devidamente formatada."
    end

# 
    # @first_column.upto(@last_column) do |column|
      # header = @spreadsheet.cell(@first_row, column)
      # if header.casecmp('contato') == 0
        # (@first_row+1).upto(@last_row) do |row| 
          # impact << @spreadsheet.cell(row, column)
        # end
      # elsif header.casecmp('datahora') == 0
        # (@first_row+1).upto(@last_row) do |row|
          # date << @spreadsheet.cell(row, column)
        # end
      # elsif header.casecmp('data') == 0
        # (@first_row+1).upto(@last_row) do |row|
          # date << @spreadsheet.cell(row, column)
        # end
      # elsif header.casecmp('hora') == 0
        # (@first_row+1).upto(@last_row) do |row|
          # hora << @spreadsheet.cell(row, column)
        # end
      # elsif header.casecmp('estado') == 0
        # (@first_row+1).upto(@last_row) do |row|
          # status << @spreadsheet.cell(row, column)
        # end
      # elsif header.casecmp('fraseologia') == 0
        # (@first_row+1).upto(@last_row) do |row|
          # phraseology << @spreadsheet.cell(row, column)
        # end
      # elsif header.casecmp('operadora') == 0
        # (@first_row+1).upto(@last_row) do |row|
          # operator << @spreadsheet.cell(row, column)
        # end
      # end
    # end
# 
# 
    # for i in 0..impact.count-1
      # info = Info.new
# 
      # info.impact = impact[i]
      # info.date = date[i]
      # info.hour = hour[i]
      # info.status = status[i]
      # info.phraseology = phraseology[i]
      # info.operator = operator[i]
      # info.campain_id = @@campain_id
      # state = State.find_by_ddd(info.impact.match("([0-9][0-9])").to_s.to_i)
      # info.region_id = state.region.id if !state.nil?
# 
      # info.save!
      # # infos << info
    # end

    # infos.each do |i|
    #   i.save!
    # end
  end

  def parse_livescreen
    
    impactColumn = nil
    dateColumn = nil
    timeColumn = nil
    phraseologyColumn = nil
    totalColumn = nil
    impressoesColumn = nil
    clicksColumn = nil
    regiaoColumn = nil
    
    # impact=Array.new
    # date=Array.new
    # time=Array.new
    # phraseology=Array.new
    # total=Array.new
    # prints=Array.new
    # clicks=Array.new
    # regions=Array.new
    infos = Array.new

   #Validação de colunas
    @first_column.upto(@last_column) do |column|
      header = @spreadsheet.cell(@first_row, column)
      if header.casecmp('contato') == 0
          impactColumn = column
      elsif header.casecmp('date') == 0
          dateColumn = column
      elsif header.casecmp('time') == 0
          timeColumn = column
      elsif header.casecmp('program') == 0
          phraseologyColumn = column
      elsif header.casecmp('total') == 0
          totalColumn = column
      elsif header.casecmp('prints') == 0
          impressoesColumn = column
      elsif header.casecmp('clicks') == 0
          clicksColumn = column
      elsif header.casecmp('region') == 0
          regiaoColumn = column
      end
    end


      query = []
      sql = nil
      i = 1
      pages = 1
      
      campainHelper = CampainTableHelper.find_by_campain_id(@@campain_id)
      if campainHelper.nil?
        campainHelper = CampainTableHelper.new(:campain_id => @@campain_id, :impacts => 0, :delivered => 0, :undelivered => 0,
        :norte => 0, :sul => 0, :nordeste => 0, :centro_oeste => 0, :sudeste => 0, :tim => 0, :vivo => 0, :oi => 0, :claro => 0)
      end

    if !dateColumn.nil? && !timeColumn.nil? && !phraseologyColumn.nil? && !totalColumn.nil? && !clicksColumn.nil? && !regiaoColumn.nil?
      (@first_row+1).upto(@last_row) do |row|
        info = Info.new
  
        info.date = @spreadsheet.cell(row, dateColumn)
        info.hour = Time.at(@spreadsheet.cell(row, timeColumn).to_i + 10800).strftime("%H:%M")
        info.phraseology = @spreadsheet.cell(row, phraseologyColumn)
        info.broadcast = @spreadsheet.cell(row, totalColumn)

        if !impressoesColumn.nil?
          info.prints = @spreadsheet.cell(row, impressoesColumn)
        end

        info.clicks = @spreadsheet.cell(row, clicksColumn)
        
        info.campain_id = @@campain_id
        region = Region.find_by_name(@spreadsheet.cell(row, regiaoColumn))
        info.region_id = region.id if !region.nil?
  
        # info.save!
        # infos << info
        if !impressoesColumn.nil?
          query << "('#{info.date}', '#{info.hour}', '#{info.phraseology}', #{info.broadcast}, #{info.prints}, #{info.clicks}, #{info.region_id}, #{info.campain_id})"
        else
          query << "('#{info.date}', '#{info.hour}', '#{info.phraseology}', #{info.broadcast}, #{info.clicks}, #{info.region_id}, #{info.campain_id})"
        end


        if i == pages*1000 || i == (@last_row-1)

          if !impressoesColumn.nil?
            sql = "INSERT INTO infos (date, hour, phraseology, broadcast, prints, clicks, region_id, campain_id) VALUES #{query.join(', ')}"
          else
            sql = "INSERT INTO infos (date, hour, phraseology, broadcast, clicks, region_id, campain_id) VALUES #{query.join(', ')}"
          end

          ActiveRecord::Base.connection.execute sql
          query = []
          sql = nil
          pages += 1
        end
        i += 1
        
      end

    else
      # ERROR!!! Coluna faltando
      session[:error_sheet] = "Nenhuma informação foi incluída no sistema. Verifique se a planilha está devidamente formatada."
    end


    # @first_column.upto(@last_column) do |column|
    # header = @spreadsheet.cell(@first_row, column)
    # if header.casecmp('contato') == 0
      # (@first_row+1).upto(@last_row) do |row|
        # # info.impact = 
        # impact << @spreadsheet.cell(row, column)
    # end
    # elsif header.casecmp('date') == 0
      # (@first_row+1).upto(@last_row) do |row|
        # # info.impact = 
        # date << @spreadsheet.cell(row, column)
    # end
    # elsif header.casecmp('time') == 0
      # (@first_row+1).upto(@last_row) do |row|
        # # info.date = 
        # time << Time.at(@spreadsheet.cell(row, column).to_i + 10800).strftime("%H:%M")
        # puts "TIME: #{time.last}"
      # end
    # elsif header.casecmp('program') == 0
      # (@first_row+1).upto(@last_row) do |row|
        # # info.status =
        # phraseology << @spreadsheet.cell(row, column)
      # end
    # elsif header.casecmp('total') == 0
      # (@first_row+1).upto(@last_row) do |row|
        # # info.phraseology =
        # total << @spreadsheet.cell(row, column)
      # end
    # elsif header.casecmp('impressoes') == 0
    # (@first_row+1).upto(@last_row) do |row|
      # # info.phraseology =
      # prints << @spreadsheet.cell(row, column)
    # end
    # elsif header.casecmp('clicks') == 0
      # (@first_row+1).upto(@last_row) do |row|
        # # info.operator =
        # clicks << @spreadsheet.cell(row, column)
      # end
#       
      # elsif header.casecmp('regiao') == 0
      # (@first_row+1).upto(@last_row) do |row|
        # # info.operator =
        # regions << @spreadsheet.cell(row, column)
      # end
    # end
# 
    # end
# 
    # for i in 0..date.count-1
      # info = Info.new
# 
      # info.impact = impact[i]
      # info.date = date[i]
      # info.hour = time[i]
      # info.phraseology = phraseology[i]
      # info.broadcast = total[i]
      # if prints[i]
        # info.prints = prints[i]
      # end
      # info.clicks = clicks[i]
      # info.campain_id = @@campain_id
      # region = Region.find_by_name(regions[i])
      # info.region_id = region.id if !region.nil?
#       
      # info.save!
      # # infos << info
    # end

    # infos.each do |i|
    #   i.save!
    # end
  end

  def check_errors
    if !session[:error_sheet].nil?
       session[:error_sheet]
     else
      0
    end

    render :partial => "sheets_error"
  end

end

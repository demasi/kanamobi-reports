$(document).ready(function() {

	var t = null;
	var f = null;
	var showDate = true;

	$.ajax({
    type: 'GET',
    url: "/campains/get_date_range",
    async: false,
    success: function(data) {
    	if (data == '') {
    		showDate = false;
    	} else {
      	f = data.split(" A ")[0].split("/");
      	t = data.split(" A ")[1].split("/");
    	}
    }
  });

	
	var to = null;
	var from = null;

	if (showDate) {
		to = new Date(t[2], (t[1]-1), t[0]);
		from = new Date(f[2], (f[1]-1), f[0]);
	} else {
		to = new Date();
		from = new Date();
	}
	
	$('#datepicker-calendar').DatePicker({
		inline: true,
		date: [from, to],
		calendars: 2,
		mode: 'range',
		current: new Date(to.getFullYear(), to.getMonth(), 1),
		onChange: function(dates,el) {
			// update the range display
			$('#date-range-field span').html(dates[0].getDate()+'/'+("0" + (dates[0].getMonth()+1)).slice(-2)+'/'+dates[0].getFullYear()+'&nbsp;&nbsp; A &nbsp;&nbsp;'+
					dates[1].getDate()+'/'+("0" + (dates[1].getMonth()+1)).slice(-2)+'/'+dates[1].getFullYear());
		}
	});
	
	// initialize the special date dropdown field
	//$('#date-range-field span').text(from.getDate()+'/'+(from.getMonth()+1)+'/'+from.getFullYear()+' A '+
	//		to.getDate()+'/'+(to.getMonth()+1)+'/'+to.getFullYear());
	
	// bind a click handler to the date display field, which when clicked
	// toggles the date picker calendar, flips the up/down indicator arrow,
	// and keeps the borders looking pretty
	$('#date-range-field').bind('click', function(){
		$('#datepicker-calendar').toggle();
		return false;
	});
	
	// global click handler to hide the widget calendar when it's open, and
	// some other part of the document is clicked.  Note that this works best
	// defined out here rather than built in to the datepicker core because this
	// particular example is actually an 'inline' datepicker which is displayed
	// by an external event, unlike a non-inline datepicker which is automatically
	// displayed/hidden by clicks within/without the datepicker element and datepicker respectively
	$('html').click(function() {
		if($('#datepicker-calendar').is(":visible")) {
			$('#datepicker-calendar').hide();

                var url = document.URL;

                url = url.replace(/^\/|\/$/g, '');
                url = url.replace(/{term}/g, '{term//}');

                var position = url.search("/date_filter");

                if( position >= 0 ) {

                    url = url.substring(0,position);

                }

                var date_filter = $('#date-range-field span').html();
                date_filter = date_filter.replace(/[&]nbsp[;]/gi, '');

                console.log(date_filter.split(" "));

                if (date_filter.split(" ")[0] != "") {
                	var from = date_filter.split(" ")[0].split("/");
									// var from = f[2] + "-"+ f[1] + "-" + f[0];
									console.log(">"+from+"<");
								} else {
									from = "data1";
								}

								if (date_filter.split(" ")[2] != "") {
									var to = date_filter.split(" ")[2].split("/");
									// var to = t[2] + "-"+ t[1] + "-" + t[0];
									console.log(">"+to+"<");
								} else {
									to = "data2";
								}

                $.ajax({
							    type: 'POST',
							    data: {from: from, to: to},
							    url: "/campains/filter_campains_by_date",
							    async: false,
							    success: function(data) {
							        $("#campains").html(data);
							      }

							    });
		}
	});
	
	// stop the click propagation when clicking on the calendar element
	// so that we don't close it
	$('#datepicker-calendar').click(function(event){
		event.stopPropagation();
	});
});

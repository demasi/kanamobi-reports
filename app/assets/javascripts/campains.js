$(document).ready(function(){

	$('#container_main').css('height', ($(window).height() - 180));

	$(window).resize(function() {
		  console.log($(window).height());
	$('#container_main').css('height', ($(window).height() - 180));
		  $('.dataTables_scrollBody').css('height', ($(window).height() - 415));
	});

  var sms = $('#sms_datatable').dataTable({
	  		  "sScrollY": ($(window).height() - 415),
		      "bScrollCollapse" : false,
		      "bAutoWidth": false,
		      "bPaginate": false,
		      "bFilter": false,
		      "bInfo": false,
		      "aoColumns": [
					{ "sWidth": "11%" },
					null,
					null,
					{ "sWidth": "50%" },
					null,
					null,
					null
			  ],
		      "oLanguage": {
					"sEmptyTable": "Não há registros."
		      },
		  	});

  var geo_midia = $('#livescreen_datatable').dataTable({
			  		  "sScrollY": ($(window).height() - 415),
				      "bScrollCollapse" : false,
				      "bAutoWidth": false,
				      "bPaginate": false,
				      "bFilter": false,
				      "bInfo": false,
				      "aoColumns": [
							{ "sWidth": "7%" },
							{ "sWidth": "7%" },
							{ "sWidth": "45%" },
							{ "sWidth": "11%" },
							{ "sWidth": "8%" },
							{ "sWidth": "8%" }
					  ],
				      "oLanguage": {
							"sEmptyTable": "Não há registros."
				      },
				  });

  var geo_midia = $('#livescreen_datatable_with_prints').dataTable({
			  		  "sScrollY": ($(window).height() - 415),
				      "bScrollCollapse" : false,
				      "bAutoWidth": false,
				      "bPaginate": false,
				      "bFilter": false,
				      "bInfo": false,
				      "aoColumns": [
							{ "sWidth": "7%" },
							{ "sWidth": "7%" },
							{ "sWidth": "45%" },
							{ "sWidth": "11%" },
							{ "sWidth": "8%" },
							{ "sWidth": "8%" },
							{ "sWidth": "12%" }
					  ],
				      "oLanguage": {
							"sEmptyTable": "Não há registros."
				      },
				  });
 
})


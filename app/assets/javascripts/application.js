// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

//= require dataTables/jquery.dataTables

$(document).ready(function() {

    // $("#select_tag_user").msDropdown();
  // $("#campains").msDropdown();
  $('#file').customFileInput();

  $(".datepicker1").datepicker();

  $("#datepickerFrom").datepicker({
    dateFormat: "dd/mm/yy",
    altFormat: "yy-mm-dd",
    altField: "#hidden-date-from"
  });

  $("#datepickerTo").datepicker({
    dateFormat: "dd/mm/yy",
    altFormat: "yy-mm-dd",
    altField: "#hidden-date-to"
  });

  $.modal.defaults = {
    overlay: "#FFF",        // Overlay color
    opacity: 0.80,          // Overlay opacity
    zIndex: 5,              // Overlay z-index.
    escapeClose: false,      // Allows the user to close the modal by pressing `ESC`
    clickClose: false,       // Allows the user to close the modal by clicking the overlay
    closeText: 'Close',     // Text content for the close <a> tag.
    showClose: true,        // Shows a (X) icon/link in the top-right corner
    modalClass: "modal",    // CSS class added to the element being displayed in the modal.
    spinnerHtml: null,      // HTML appended to the default spinner during AJAX requests.
    showSpinner: true       // Enable/disable the default spinner during AJAX requests.
  };

  showLinkToAddCampain();
  

  $('#datatable').dataTable();

  $("#link").click(function(e) {
    console.log('a');
  });

  $('#nav li').hover(
    function () {
        $('ul', this).stop(true, true).slideDown(200);
    },
    function () {
        $('ul', this).stop(true, true).slideUp(200);            
  });

   $.get("/sheets/check_errors", function(data){
      if (data != 0 && data.charAt(0) != '<') {
        alert(data);
      };
    });
});

function selectUserCampain(value) {
  // console.log(">>"+value);


   if (value == 0) { 
      $("#bar").addClass("hidden");
      $("#campain-filter").addClass("hidden");
      $("#date-period").addClass("hidden");
       document.getElementById('client-name').innerHTML="CADASTRAR CLIENTE";
      $.get("/users/redirect_to_user_path/");
      return null;
   };

   $.get("/users/select_user_campain/" + value, function(data){
        $("#campains").html(data);
  	    var campainIndex = parseInt($("#campains option:last").attr("value"));
  	    
  	    if (campainIndex > 0){
  		    $("#bar").removeClass("hidden");
  	    
  	    }else{
  		    $("#bar").addClass("hidden");
  	    }
    });
    
    var clientNameSpan = document.getElementById('client-name');
    if (clientNameSpan != null) {
      $.get("/users/select_user_name/" + value, function(data){
        document.getElementById('client-name').innerHTML=data;
      });
    };

    $.get("/users/select_user_logo/" + value, function(data){
      if (data.indexOf('/assets/') !== -1) {
        $("#selected_client_logo").html("");
        return;
      };
      $("#selected_client_logo").html(data);
    });

    $.get("/campains/show_date_filter/" + value, function(data){
      document.getElementById('date-range-field').innerHTML=data;
    });

    $.get("/users/redirecionar/" + value);
    

    $("#campain-filter").removeClass("hidden");
    $("#date-period").removeClass("hidden");

}

function redirect(value) {
      if (value == 0) {return null};

      $(window.location.replace("/campains/"+value));
}

function createNewCampains() {
  var value = $("#select_tag_user option:selected").attr('value');
  // console.log("adasdsda >> " + value);

  $.ajax({
    type: 'POST',
    data: {user: value},
    url: "/users/get_data",
    async: false

    });
}

function resetSelects() {
  $("#select_tag_user").val($("#select_tag_user option:first"));
}

function showLinkToAddCampain () {
  var selectedId = $("#select_tag_user option:selected").attr("value");
  if (selectedId == null) {
    var clientName = $("span#client-name").html();
    $("#bar").removeClass("hidden");
    $("#campain-filter").removeClass("hidden");
    $.get("/campains/show_date_filter_by_user_name/" + clientName, function(data){
      document.getElementById('date-range-field').innerHTML=data;
    });
    return;
  }
  // selectUserCampain(selectedId);
  if (parseInt(selectedId) != 0) {
    $.get("/users/select_user_campain/" + selectedId, function(data){
        $("#campains").html(data);
        var campainIndex = parseInt($("#campains option:last").attr("value"));
        
        if (campainIndex > 0){
          $("#bar").removeClass("hidden");
        
        }else{
          $("#bar").addClass("hidden");
        }
    });
    
    var clientNameSpan = document.getElementById('client-name');
    if (clientNameSpan != null) {
      $.get("/users/select_user_name/" + selectedId, function(data){
        document.getElementById('client-name').innerHTML=data;
      });
    };

    $.get("/users/select_user_logo/" + selectedId, function(data){
      if (data.indexOf('/assets/') !== -1) {
        $("#selected_client_logo").html("");
        return;
      };
      $("#selected_client_logo").html(data);
    });

    $.get("/campains/show_date_filter/" + selectedId, function(data){
      document.getElementById('date-range-field').innerHTML=data;
    });
    

    $("#campain-filter").removeClass("hidden");
    $("#date-period").removeClass("hidden");
  } else {
    $("#bar").addClass("hidden");
    $("#campain-filter").addClass("hidden");
    $("#date-period").addClass("hidden");
     document.getElementById('client-name').innerHTML="CADASTRAR CLIENTE";
    $.get("/users/redirect_to_user_path/");
    return null;
  }
}


# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130228183712) do

  create_table "campain_table_helpers", :force => true do |t|
    t.integer  "campain_id"
    t.integer  "impacts"
    t.integer  "delivered"
    t.integer  "undelivered"
    t.integer  "norte"
    t.integer  "sul"
    t.integer  "sudeste"
    t.integer  "nordeste"
    t.integer  "centro_oeste"
    t.integer  "tim"
    t.integer  "vivo"
    t.integer  "claro"
    t.integer  "oi"
    t.integer  "date_range"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "campains", :force => true do |t|
    t.string   "name"
    t.datetime "from"
    t.datetime "to"
    t.integer  "campain_type"
    t.integer  "user_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "infos", :force => true do |t|
    t.string   "impact"
    t.datetime "date"
    t.string   "hour"
    t.string   "phraseology"
    t.string   "status"
    t.string   "operator"
    t.integer  "broadcast"
    t.integer  "clicks"
    t.integer  "prints"
    t.integer  "campain_id"
    t.integer  "region_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "regions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sheets", :force => true do |t|
    t.string   "path"
    t.integer  "campain_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "states", :force => true do |t|
    t.integer  "ddd"
    t.string   "name"
    t.string   "alias"
    t.integer  "region_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "contact"
    t.string   "logo"
    t.string   "role",          :default => "user"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

end

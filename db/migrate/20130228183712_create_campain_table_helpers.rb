class CreateCampainTableHelpers < ActiveRecord::Migration
  def change
    create_table :campain_table_helpers do |t|
      t.integer :campain_id
      t.integer :impacts
      t.integer :delivered
      t.integer :undelivered
      t.integer :norte
      t.integer :sul
      t.integer :sudeste
      t.integer :nordeste
      t.integer :centro_oeste
      t.integer :tim
      t.integer :vivo
      t.integer :claro
      t.integer :oi
      t.integer :date_range

      t.timestamps
    end
  end
end

class CreateCampains < ActiveRecord::Migration
  def change
    create_table :campains do |t|
      t.string :name
      t.datetime :from
      t.datetime :to
      t.integer :campain_type
      t.integer :user_id

      t.timestamps
    end
  end
end

class CreateInfos < ActiveRecord::Migration
  def change
    create_table :infos do |t|
      t.string :impact
      t.datetime :date
      t.string :hour
      t.string :phraseology
      t.string :status
      t.string :operator
      t.integer :broadcast
      t.integer :clicks
      t.integer :prints
      t.integer :campain_id
      t.integer :region_id

      t.timestamps
    end
  end
end

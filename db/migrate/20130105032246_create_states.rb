class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.integer :ddd
      t.string :name
      t.string :alias
      t.integer :region_id

      t.timestamps
    end
  end
end

class CreateSheets < ActiveRecord::Migration
  def change
    create_table :sheets do |t|
      t.string :path
      t.integer :campain_id

      t.timestamps
    end
  end
end
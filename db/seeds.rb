# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Emanuel', :city => cities.first)

puts 'Cadastrando Regioes...'

norte = Region.create(:name => "Norte")
nordeste = Region.create(:name => "Nordeste")
centro_oeste = Region.create(:name => "Centro Oeste")
sudeste = Region.create(:name => "Sudeste")
sul = Region.create(:name => "Sul")

puts 'Regioes cadastradas.'
puts 'Cadastrando Estados...'

State.create(
	[
		{:name => "Acre", :alias => "AC", :ddd => 68, :region_id => norte.id},
		{:name => "Alagoas", :alias => "AL", :ddd => 82, :region_id => nordeste.id},
		{:name => "Amapá", :alias => "AP", :ddd => 96, :region_id => norte.id},
		{:name => "Amazonas", :alias => "AM", :ddd => 92, :region_id => norte.id},
    {:name => "Amazonas", :alias => "AM", :ddd => 97, :region_id => norte.id},
    {:name => "Bahia", :alias => "BA", :ddd => 71, :region_id => nordeste.id},
    {:name => "Bahia", :alias => "BA", :ddd => 73, :region_id => nordeste.id},
    {:name => "Bahia", :alias => "BA", :ddd => 74, :region_id => nordeste.id},
    {:name => "Bahia", :alias => "BA", :ddd => 75, :region_id => nordeste.id},
    {:name => "Bahia", :alias => "BA", :ddd => 77, :region_id => nordeste.id},
    {:name => "Ceará", :alias => "CE", :ddd => 85, :region_id => nordeste.id},
    {:name => "Ceará", :alias => "CE", :ddd => 88, :region_id => nordeste.id},
		{:name => "Distrito Federal", :alias => "DF", :ddd => 61, :region_id => centro_oeste.id},
    {:name => "Espírito Santo", :alias => "ES", :ddd => 27, :region_id => sudeste.id},
    {:name => "Espírito Santo", :alias => "ES", :ddd => 28, :region_id => sudeste.id},
    {:name => "Goiás", :alias => "GO", :ddd => 61, :region_id => centro_oeste.id},
    {:name => "Goiás", :alias => "GO", :ddd => 62, :region_id => centro_oeste.id},
    {:name => "Goiás", :alias => "GO", :ddd => 64, :region_id => centro_oeste.id},
    {:name => "Maranhão", :alias => "MA", :ddd => 98, :region_id => nordeste.id},
    {:name => "Maranhão", :alias => "MA", :ddd => 99, :region_id => nordeste.id},
    {:name => "Mato Grosso", :alias => "MT", :ddd => 65, :region_id => centro_oeste.id},
    {:name => "Mato Grosso", :alias => "MT", :ddd => 66, :region_id => centro_oeste.id},
    {:name => "Mato Grosso do Sul", :alias => "MS", :ddd => 67, :region_id => centro_oeste.id},
    {:name => "Minas Gerais", :alias => "MG", :ddd => 31, :region_id => centro_oeste.id},
    {:name => "Minas Gerais", :alias => "MG", :ddd => 32, :region_id => centro_oeste.id},
    {:name => "Minas Gerais", :alias => "MG", :ddd => 33, :region_id => centro_oeste.id},
    {:name => "Minas Gerais", :alias => "MG", :ddd => 34, :region_id => centro_oeste.id},
    {:name => "Minas Gerais", :alias => "MG", :ddd => 35, :region_id => centro_oeste.id},
    {:name => "Minas Gerais", :alias => "MG", :ddd => 37, :region_id => centro_oeste.id},
    {:name => "Minas Gerais", :alias => "MG", :ddd => 38, :region_id => centro_oeste.id},
		{:name => "Pará", :alias => "PA", :ddd => 91, :region_id => norte.id},
    {:name => "Pará", :alias => "PA", :ddd => 93, :region_id => norte.id},
    {:name => "Pará", :alias => "PA", :ddd => 94, :region_id => norte.id},
		{:name => "Paraíba", :alias => "PB", :ddd => 83, :region_id => nordeste.id},
		{:name => "Paraná", :alias => "PR", :ddd => 41, :region_id => sul.id},
    {:name => "Paraná", :alias => "PR", :ddd => 42, :region_id => sul.id},
    {:name => "Paraná", :alias => "PR", :ddd => 43, :region_id => sul.id},
    {:name => "Paraná", :alias => "PR", :ddd => 44, :region_id => sul.id},
    {:name => "Paraná", :alias => "PR", :ddd => 45, :region_id => sul.id},
    {:name => "Paraná", :alias => "PR", :ddd => 46, :region_id => sul.id},
    {:name => "Pernambuco", :alias => "PE", :ddd => 81, :region_id => nordeste.id},
    {:name => "Pernambuco", :alias => "PE", :ddd => 87, :region_id => nordeste.id},
    {:name => "Piauí", :alias => "PI", :ddd => 86, :region_id => nordeste.id},
    {:name => "Piauí", :alias => "PI", :ddd => 89, :region_id => nordeste.id},
		{:name => "Rio de Janeiro", :alias => "RJ", :ddd => 21, :region_id => sudeste.id},
    {:name => "Rio de Janeiro", :alias => "RJ", :ddd => 22, :region_id => sudeste.id},
    {:name => "Rio de Janeiro", :alias => "RJ", :ddd => 24, :region_id => sudeste.id},
		{:name => "Rio Grande do Norte", :alias => "RN", :ddd => 84, :region_id => nordeste.id},
		{:name => "Rio Grande do Sul", :alias => "RS", :ddd => 51, :region_id => sul.id},
    {:name => "Rio Grande do Sul", :alias => "RS", :ddd => 53, :region_id => sul.id},
    {:name => "Rio Grande do Sul", :alias => "RS", :ddd => 54, :region_id => sul.id},
    {:name => "Rio Grande do Sul", :alias => "RS", :ddd => 55, :region_id => sul.id},
		{:name => "Rondônia", :alias => "RO", :ddd => 69, :region_id => norte.id},
		{:name => "Roraima", :alias => "RR", :ddd => 95, :region_id => norte.id},
		{:name => "Santa Catarina", :alias => "SC", :ddd => 47, :region_id => sul.id},
    {:name => "Santa Catarina", :alias => "SC", :ddd => 48, :region_id => sul.id},
    {:name => "Santa Catarina", :alias => "SC", :ddd => 49, :region_id => sul.id},
		{:name => "São Paulo", :alias => "SP", :ddd => 11, :region_id => sudeste.id},
    {:name => "São Paulo", :alias => "SP", :ddd => 12, :region_id => sudeste.id},
    {:name => "São Paulo", :alias => "SP", :ddd => 13, :region_id => sudeste.id},
    {:name => "São Paulo", :alias => "SP", :ddd => 14, :region_id => sudeste.id},
    {:name => "São Paulo", :alias => "SP", :ddd => 15, :region_id => sudeste.id},
    {:name => "São Paulo", :alias => "SP", :ddd => 16, :region_id => sudeste.id},
    {:name => "São Paulo", :alias => "SP", :ddd => 17, :region_id => sudeste.id},
    {:name => "São Paulo", :alias => "SP", :ddd => 18, :region_id => sudeste.id},
    {:name => "São Paulo", :alias => "SP", :ddd => 19, :region_id => sudeste.id},
		{:name => "Sergipe", :alias => "SE", :ddd => 79, :region_id => nordeste.id},
		{:name => "Tocantins", :alias => "TO", :ddd => 63, :region_id => norte.id}
	]
)

puts 'Estados cadastrados.'

User.create(:email => "kanamobi.contato@gmail.com", :password => "kanamobi", :role => "admin", :name => "Administrador")
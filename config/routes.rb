Kanamobi::Application.routes.draw do
  
  resources :infos


  get "log_in" => "sessions#new", :as => "log_in"
  get "log_out" => "sessions#destroy", :as => "log_out"
  get "sign_up" => "users#new", :as => "sign_up"
  get "forgot_password" => "users#forgot_password", :as => "forgot_password"
  match "users/send_password_email" => "users#send_password_email", :as => "send_password_email"
  match "users/associate_user_to_campain" => "users#associate_user_to_campain", :as => "associate_user_to_campain"
  match "users/get_data" => "users#get_data", :as => "get_data"
  match "users/select_user_campain/:id" => "users#select_user_campain", :as => "select_user_campain"
  match "users/redirect_to_user_path/" => "users#redirect_to_user_path", :as => "redirect_to_user_path"
  match "users/select_user_name/:id" => "users#select_user_name", :as => "select_user_name"
  match "users/select_user_logo/:id" => "users#select_user_logo", :as => "select_user_logo"
  match "users/delete_logo/:id" => "users#delete_logo", :as => "delete_logo"
  match "users/redirecionar/:id" => "users#redirecionar", :as => "redirecionar"
  match "users/update_password" => "users#update_password", :as => "update_password"
  match "users/update_password_action" => "users#update_password_action", :as => "update_password_action"
  match "campains/filter_campains_by_date" => "campains#filter_campains_by_date", :as => "filter_campains_by_date"
  match "campains/show_date_filter/:id" => "campains#show_date_filter", :as => "show_date_filter"
  match "campains/show_date_filter_by_user_name/:name" => "campains#show_date_filter_by_user_name", :as => "show_date_filter_by_user_name"
  match "campains/get_svg" => "campains#get_svg", :as => "get_svg"
  match "campains/get_date_range" => "campains#get_date_range", :as => "get_date_range"
  match 'export_spreadsheet/:id' => 'campains#export_spreadsheet', :as => :export_spreadsheet
  match "sheets/check_errors" => "sheets#check_errors", :as => "check_errors"

  root :to => "sessions#new"

  resources :users
  resources :sessions
  resources :campains
  resources :sheets

end
